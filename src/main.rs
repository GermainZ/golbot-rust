extern crate regex;
extern crate toml;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate lazy_static;

use regex::Regex;
use toml::Value;

use std::io::prelude::*;
use std::io::Error;
use std::net::TcpStream;

use std::sync::RwLock;
//use std::sync::Mutex;
use std::collections::VecDeque;
use std::sync::Arc;
use std::thread;

use std::boxed::Box;
use std::time::Duration;

use std::fs::File;
use std::path::Path;

mod plugin;
use self::plugin::*;

use self::plugin::pingpong::PingPong;
use self::plugin::rss_plugin::RssReader;
//use plugin::hello::Hello;
use self::plugin::seen::LastSeen;
use self::plugin::tell::Tell;
use self::plugin::url_reader::UrlReader;

#[derive(Deserialize)]
pub struct BotConfig {
    nick: String,
    serv: String,
    channel: String,
    realname: String,
    ident: String,
    password: Option<String>,
}

impl BotConfig {
    fn new(
        nick: &str,
        serv: &str,
        channel: &str,
        realname: &str,
        ident: &str,
        password: &str,
    ) -> BotConfig {
        return BotConfig {
            nick: String::from(nick),
            serv: String::from(serv),
            channel: String::from(channel),
            realname: String::from(realname),
            ident: String::from(ident),
            password: Some(String::from(password)),
        };
    }

    fn debug_defaults() -> BotConfig {
        return BotConfig::new("GOLBOTv3", "chat.freenode.net", "#golbottest", "", "", "");
    }
}

fn send_sock(sock: &mut TcpStream, msg: String) -> Result<usize, Error> {
    sock.write(msg.as_bytes())
}

fn recv_sock(sock: &mut TcpStream) -> Result<String, String> {
    let mut buffer: [u8; 1024] = [0 as u8; 1024];

    let res = sock.read(&mut buffer);

    let mut vec_buffer: Vec<u8> = Vec::new();

    for byte in buffer.iter() {
        vec_buffer.push(byte.clone());
    }

    let msg_result = String::from_utf8(vec_buffer);

    if msg_result.is_err() {
        return Ok(String::from("incorrect_utf8"));
    }

    let mut msg = msg_result.unwrap();

    match res {
        Ok(size) => {
            if size == 0 {
                println!("We should be cutting the connection here.");
            }

            msg.split_off(size);
            Ok(msg)
        }
        Err(_) => Err(String::from("Failed to read!")),
    }
}

fn form_chan_msg(conf: &BotConfig, msg: String) -> String {
    return format!("PRIVMSG {} :{}\r\n", conf.channel, msg);
}

fn parse_chan_msg(msg: &String) -> Option<(String, String, String)> {
    if msg.contains("PRIVMSG") {
        lazy_static! {
            static ref CHAN_MSG_REGEX: Regex = Regex::new(r":(.+)!.+(#.*) :(.+)").unwrap();
        }

        println!("{}", &msg);

        let chan_msg = CHAN_MSG_REGEX.captures(msg);

        if chan_msg.is_some() {
            println!("This is a chan message");
            let chan_msg = chan_msg.unwrap();

            let nick = chan_msg.get(1).unwrap();
            let chan = chan_msg.get(2).unwrap();
            let msg = chan_msg.get(3).unwrap();

            return Some((
                String::from(nick.as_str()),
                String::from(chan.as_str()),
                String::from(msg.as_str()),
            ));
        }
    }

    return None;
}

fn read_bot_config(file: &Path) -> Option<BotConfig> {
    if file.is_file() {
        let mut fob = File::open(file).unwrap();

        let mut text = String::new();
        fob.read_to_string(&mut text);

        let result: Result<BotConfig, toml::de::Error> = toml::from_str(text.as_str());

        match result {
            Ok(config) => return Some(config),
            Err(e) => {
                println!("Couldn't load config file: {}", e);
                return None;
            }
        }
    }

    println!("Couldn't load config file: File not found.");
    return None;
}

fn read_plugin_config(file: &Path) -> Option<Value> {
    if file.is_file() {
        let mut fob = File::open(file).unwrap();

        let mut text = String::new();
        fob.read_to_string(&mut text);

        return Some(text.as_str().parse::<Value>().unwrap());
    } else {
        return None;
    }
}

struct IRCBot {
    config: Arc<BotConfig>,
    pl_config: Arc<Value>,
    plugins: Vec<Box<dyn Plugin>>,
    a_plugins: Vec<Box<dyn AsyncPlugin>>,
    in_queue: Arc<RwLock<VecDeque<String>>>,
    out_queue: Arc<RwLock<VecDeque<String>>>,
    running: Arc<RwLock<bool>>,
}

impl IRCBot {
    fn new(config: Arc<BotConfig>, pl_config: Arc<Value>) -> IRCBot {
        return IRCBot {
            config: config,
            pl_config: pl_config,
            plugins: Vec::new(),
            a_plugins: Vec::new(),

            in_queue: Arc::new(RwLock::new(VecDeque::new())),
            out_queue: Arc::new(RwLock::new(VecDeque::new())),
            running: Arc::new(RwLock::new(false)),
        };
    }

    fn send(&self, msg: String) {
        let mut queue = self.out_queue.write().unwrap();

        println!("-> {}", &msg);

        queue.push_back(msg);
    }

    fn recv(&self) -> Option<String> {
        let mut queue = self.in_queue.write().unwrap();

        let msg = queue.pop_front();

        match msg {
            Some(s) => {
                println!("<- {}", s);
                return Some(s);
            }
            None => return None,
        }
    }

    fn sender(
        out_queue: Arc<RwLock<VecDeque<String>>>,
        running: Arc<RwLock<bool>>,
        socket: TcpStream,
    ) {
        let mut sock = socket;
        let check_running = running;

        loop {
            {
                let running = check_running.read().unwrap();

                if *running == false {
                    println!("Sender exited!");
                    panic!();
                }
            }

            {
                let mut queue = out_queue.write().unwrap();

                let line = queue.pop_front();

                match line {
                    Some(s) => {
                        let _result = send_sock(&mut sock, s);
                    }
                    None => (),
                }
            }

            thread::sleep(Duration::from_millis(50));
        }
    }

    fn receiver(
        in_queue: Arc<RwLock<VecDeque<String>>>,
        running: Arc<RwLock<bool>>,
        socket: TcpStream,
    ) {
        let mut sock = socket;
        let check_running = running;

        loop {
            {
                let running = check_running.read().unwrap();

                if *running == false {
                    println!("Receiver exited!");
                    panic!();
                }
            }

            let line = recv_sock(&mut sock);

            match line {
                Ok(s) => {
                    let mut queue = in_queue.write().unwrap();
                    queue.push_back(s);
                }
                Err(_e) => {
                    let mut queue = in_queue.write().unwrap();
                    queue.push_back(String::from(""));
                }
            }

            thread::sleep(Duration::from_millis(50));
        }
    }

    fn add_plugin(&mut self, plugin: Box<dyn Plugin>) {
        self.plugins.push(plugin);
    }

    fn add_async_plugin(&mut self, plugin: Box<dyn AsyncPlugin>) {
        self.a_plugins.push(plugin);
    }

    fn start(&mut self) -> Result<i32, String> {
        println!("Connecting to the server...");

        let socket = TcpStream::connect(format!("{}:6667", &self.config.serv))
            .expect("Couldn't connect to server!");

        self.send(format!("NICK {}\r\n", self.config.nick));
        self.send(format!(
            "USER {} {} bla :{}\r\n",
            self.config.ident, self.config.serv, self.config.realname
        ));

        let sock2 = socket.try_clone().unwrap();

        let in_queue = self.in_queue.clone();
        let out_queue = self.out_queue.clone();

        {
            let mut running = self.running.write().unwrap();
            *running = true;
        }

        let running1 = self.running.clone();
        let running2 = self.running.clone();

        thread::spawn(move || IRCBot::sender(out_queue, running1.clone(), socket));
        thread::spawn(move || IRCBot::receiver(in_queue, running2.clone(), sock2));

        match &self.config.password {
            Some(password) => {
                self.send(format!(
                    "PRIVMSG NickServ :identify {} {}\r\n",
                    self.config.nick, password
                ));

                loop {
                    {
                        let msg = self.recv();

                        match msg {
                            Some(message) => {
                                if message.contains("You are now identified for ") {
                                    break;
                                }
                            }
                            None => (),
                        }
                    }

                    thread::sleep(Duration::from_millis(20));
                }
            }
            None => (),
        }

        self.send(format!("JOIN {}\r\n", self.config.channel));

        for plugin in &self.a_plugins {
            plugin.start();
        }

        'mainloop: loop {
            let message = self.recv();

            match message {
                Some(line) => {
                    if line.starts_with("ERROR:") {
                        let mut running = self.running.write().unwrap();
                        *running = false;

                        for plugin in &self.a_plugins {
                            plugin.stop();
                        }

                        return Err(String::from("An error occured, line severed."));
                    } else if line.len() == 0 {
                        let mut running = self.running.write().unwrap();
                        *running = false;

                        for plugin in &self.a_plugins {
                            plugin.stop();
                        }

                        return Err(String::from(
                            "Received an empty message, likely broken connection.",
                        ));
                    }

                    if line.len() > 0 {
                        let mut responses: Vec<String> = Vec::new();

                        for pl in &mut self.plugins {
                            let pl_resp = pl.process_line(&line);

                            match pl_resp {
                                Some(msg) => {
                                    responses.push(msg);
                                }
                                None => (),
                            }
                        }

                        for resp in responses {
                            self.send(resp);
                        }
                    }
                }
                None => (),
            }

            thread::sleep(Duration::from_millis(50));
        }

        let mut running = self.running.write().unwrap();
        *running = false;

        return Ok(0);
    }
}

fn main() {
    let config = Arc::new(read_bot_config(Path::new("botconfig.toml")).unwrap());
    let pl_config = Arc::new(read_plugin_config(Path::new("plugins.toml")).unwrap());

    println!("{}", pl_config.to_string());

    loop {
        let mut bot = IRCBot::new(config.clone(), pl_config.clone());

        let conf = bot.config.clone();
        let pl_conf = bot.pl_config.clone();
        let out_queue = bot.out_queue.clone();

        bot.add_plugin(Box::new(PingPong::new()));
        bot.add_plugin(Box::new(LastSeen::new(conf.clone())));
        bot.add_plugin(Box::new(UrlReader::new(conf.clone(), pl_conf.clone())));
        bot.add_plugin(Box::new(Tell::new(conf.clone())));

        bot.add_async_plugin(Box::new(RssReader::new(
            conf.clone(),
            pl_conf.clone(),
            out_queue.clone(),
        )));

        let resp = bot.start();

        match resp {
            Err(e) => println!("{}", e),
            Ok(_a) => {
                println!("Bot exited gracefully.");
                return ();
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn recognizes_channel_message() {
        let _config = BotConfig::debug_defaults();

        let message = String::from(":testing!test@testing PRIVMSG #golbottest :hello");

        assert!(parse_chan_msg(&message).is_some());
    }

    #[test]
    fn form_chan_msg_is_valid() {
        let config = BotConfig::debug_defaults();

        let msg = form_chan_msg(&config, String::from("Hello, world!"));

        assert_eq!(msg, String::from("PRIVMSG #golbottest :Hello, world!\r\n"));
    }
}
