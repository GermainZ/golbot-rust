use std::collections::VecDeque;
use std::sync::Arc;
use std::sync::RwLock;

use super::*;

pub mod pingpong;
pub mod rss_plugin;
//pub mod hello;
pub mod seen;
pub mod tell;
pub mod url_reader;

pub trait Plugin {
    fn name(&self) -> String;
    fn process_line(&mut self, line: &String) -> Option<String>;
}

pub trait AsyncPlugin {
    fn name(&self) -> String;
    fn give_in_queue(&mut self, in_queue: Arc<RwLock<VecDeque<String>>>);
    fn give_out_queue(&mut self, out_queue: Arc<RwLock<VecDeque<String>>>);
    fn start(&self);
    fn stop(&self);
}
