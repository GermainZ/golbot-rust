extern crate chrono;

use super::*;

use self::chrono::prelude::*;
use std::collections::HashMap;

pub struct Tell {
    config: Arc<BotConfig>,
    msg_db: HashMap<String, Vec<(String, String, DateTime<Utc>)>>,
}

impl Tell {
    pub fn new(config: Arc<BotConfig>) -> Tell {
        return Tell {
            config: config,
            msg_db: HashMap::new(),
        };
    }
}

impl Plugin for Tell {
    fn name(&self) -> String {
        return String::from("LastSeen");
    }

    fn process_line(&mut self, line: &String) -> Option<String> {
        let line_clone = line.clone();
        let msg = parse_chan_msg(&line_clone);

        match msg {
            Some((nick, _chan, msg)) => {
                lazy_static! {
                    static ref TELL_REGEX: Regex = Regex::new(r"(.+). tell (\w+) (.+)").unwrap();
                }

                let msg_match = TELL_REGEX.captures(msg.as_str());

                if msg_match.is_some() {
                    let msg_match = msg_match.unwrap();

                    let my_nick_match = msg_match.get(1).unwrap();
                    let receiver_nick_match = msg_match.get(2).unwrap();
                    let msg_to_send_match = msg_match.get(3).unwrap();

                    let my_nick = String::from(my_nick_match.as_str());
                    let receiver_nick = String::from(receiver_nick_match.as_str());
                    let msg_to_send = String::from(msg_to_send_match.as_str());

                    if my_nick != self.config.nick {
                        return None;
                    }

                    if self.msg_db.contains_key(&receiver_nick) {
                        let messages = self.msg_db.get_mut(&receiver_nick).unwrap();

                        messages.push((nick.clone(), msg_to_send, Utc::now()));
                    } else {
                        self.msg_db.insert(
                            String::from(receiver_nick.as_str()),
                            vec![(nick.clone(), String::from(msg_to_send.as_str()), Utc::now())],
                        );
                    }

                    return Some(form_chan_msg(
                        &self.config,
                        format!("{}, Okay, I'll pass your message along!", nick),
                    ));
                } else {
                    if self.msg_db.contains_key(&nick) {
                        let messages = self.msg_db.get_mut(&nick).unwrap();

                        if messages.len() > 0 {
                            let (sender, message, time) = messages.pop().unwrap();

                            return Some(form_chan_msg(
                                &self.config,
                                format!(
                                    "{}, {} left you a message at {}: {}",
                                    &nick,
                                    &sender,
                                    &time.format("%Y-%m-%d %H:%M:%S UTC").to_string(),
                                    &message
                                ),
                            ));
                        }
                    }
                }
            }
            None => return None,
        }

        return None;
    }
}
