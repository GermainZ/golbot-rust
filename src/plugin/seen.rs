extern crate chrono;

use super::*;

use self::chrono::prelude::*;
use std::collections::HashMap;

pub struct LastSeen {
    config: Arc<BotConfig>,
    seen_db: HashMap<String, (String, DateTime<Utc>)>,
}

impl LastSeen {
    pub fn new(config: Arc<BotConfig>) -> LastSeen {
        return LastSeen {
            config: config,
            seen_db: HashMap::new(),
        };
    }
}

impl Plugin for LastSeen {
    fn name(&self) -> String {
        return String::from("LastSeen");
    }

    fn process_line(&mut self, line: &String) -> Option<String> {
        let line_clone = line.clone();
        let msg = parse_chan_msg(&line_clone);

        match msg {
            Some((nick, _chan, msg)) => {
                if msg.starts_with("seen ") {
                    let split: Vec<&str> = msg.split_whitespace().collect();

                    if split.len() > 1 {
                        let nick = String::from(split[1]);

                        let result = self.seen_db.get(&nick);

                        match result {
                            Some((msg, date_time)) => {
                                return Some(form_chan_msg(
                                    &self.config,
                                    format!(
                                        "I last saw {} at {} saying: {}",
                                        &nick,
                                        &date_time.format("%Y-%m-%d %H:%M:%S UTC").to_string(),
                                        &msg
                                    ),
                                ))
                            }
                            None => return None,
                        }
                    }
                } else {
                    self.seen_db.insert(nick, (msg, Utc::now()));
                }
            }
            None => return None,
        }

        return None;
    }
}
