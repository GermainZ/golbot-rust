extern crate rss;

extern crate ureq;

use super::*;

use chrono::prelude::Utc;
use chrono::Duration as ChronoDuration;

use std::io::Read;
use std::io::BufReader;

use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;

pub struct RssReader {
    config: Arc<BotConfig>,
    out_queue: Arc<RwLock<VecDeque<String>>>,
    rss_feed: Arc<String>,
    rss_delay: Arc<i64>,
    running: Arc<AtomicBool>,
}

impl RssReader {
    pub fn new(
        config: Arc<BotConfig>,
        pl_config: Arc<Value>,
        out_queue: Arc<RwLock<VecDeque<String>>>,
    ) -> RssReader {
        let feed = pl_config.get("rss");
        let mut rss_feed: String = String::from("http://lorem-rss.herokuapp.com/feed");
        let mut rss_delay: i64 = 0;

        if let Some(val) = feed {
            if val.is_str() {
                rss_feed = String::from(val.as_str().unwrap());
            }
        }

        let delay = pl_config.get("rss_delay");

        if let Some(val) = delay {
            if val.is_integer() {
                rss_delay = val.as_integer().unwrap();
            }
        }

        return RssReader {
            config: config,
            out_queue: out_queue,
            rss_feed: Arc::new(rss_feed),
            rss_delay: Arc::new(rss_delay),
            running: Arc::new(AtomicBool::new(false)),
        };
    }
}

impl AsyncPlugin for RssReader {
    fn name(&self) -> String {
        return String::from("RSS");
    }

    fn give_in_queue(&mut self, _in_queue: Arc<RwLock<VecDeque<String>>>) {}

    fn give_out_queue(&mut self, out_queue: Arc<RwLock<VecDeque<String>>>) {
        self.out_queue = out_queue;
    }

    fn start(&self) {
        let running = self.running.clone();

        if running.load(Ordering::Relaxed) {
            return;
        }

        let out_queue = self.out_queue.clone();
        let config = self.config.clone();
        let feed = self.rss_feed.clone();
        let delay = self.rss_delay.clone();

        running.store(true, Ordering::Relaxed);

        thread::spawn(move || {
            let mut last_posted: i64 = 0;
            let mut discarded = false;

            println!("Started RSS plugin");

            'rss: loop {
                {
                    let check_running = running.load(Ordering::Relaxed);

                    if check_running == false {
                        println!("RSS: Main app exited, I'm outta here.");
                        break 'rss;
                    }
                }

                let resp = ureq::get(feed.as_str()).call();

                let rss_channel_result = rss::Channel::read_from(BufReader::new(resp.into_reader()));

                if rss_channel_result.is_ok() {
                    let current_time = (Utc::now() - ChronoDuration::minutes(*delay)).timestamp();

                    let mut rss_channel = rss_channel_result.unwrap();

                    let items = rss_channel.items_mut();
                    items.reverse();

                    for item in items.iter() {
                        let date = item.pub_date();

                        match date {
                            None => (),
                            Some(s) => {
                                let date_time = chrono::DateTime::parse_from_rfc2822(s).unwrap();

                                if date_time.timestamp() > last_posted
                                    && date_time.timestamp() < current_time
                                {
                                    last_posted = date_time.timestamp();

                                    if discarded {
                                        let mut queue = out_queue.write().unwrap();

                                        let output = form_chan_msg(
                                            &config,
                                            format!(
                                                "RSS: {} {}",
                                                item.title().unwrap(),
                                                item.link().unwrap()
                                            ),
                                        );

                                        println!("RSS: -> {}", &output);

                                        queue.push_back(output);
                                        thread::sleep(Duration::from_secs(5));
                                    }
                                }
                            }
                        }
                    }

                    if !discarded {
                        discarded = true;
                    }
                }
                thread::sleep(Duration::from_secs(10));
            }
        });
    }

    fn stop(&self) {
        println!("Stopping RSS");

        self.running.store(false, Ordering::Relaxed);
    }
}
