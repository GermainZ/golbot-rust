use super::*;

pub struct PingPong {}

impl PingPong {
    pub fn new() -> PingPong {
        return PingPong {};
    }
}

impl Plugin for PingPong {
    fn name(&self) -> String {
        return String::from("PingPong");
    }

    fn process_line(&mut self, line: &String) -> Option<String> {
        let line_clone = line.clone();
        let split_line: Vec<&str> = line_clone.split_whitespace().collect();

        if split_line.len() > 1 && split_line[0] == "PING" {
            return Some(format!("PONG {}\r\n", split_line[1]));
        }

        return None;
    }
}
